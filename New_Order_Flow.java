package Java_programmes;

import java.awt.AWTException;
import java.awt.Robot;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class New_Order_Flow {

	public static void main(String[] args) throws InterruptedException, AWTException {
		
		FirefoxDriver driver=new FirefoxDriver();
		driver.get("http://dev.devnagri.co.in/");
		driver.manage().window().maximize();
		//login
		driver.findElementByXPath("//*[@id=\"main-wrapper\"]/header/nav/div[2]/a").click();
		driver.findElementByXPath("//*[@id=\"email1\"]").sendKeys("test.client@devnagri.com");
		driver.findElementByXPath("//*[@id=\"password1\"]").sendKeys("secret");
		driver.findElementByXPath("//*[@id=\"loginform\"]/div[4]/div/button").click();
		//Order flow
		driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div/div[2]/div/div/section[2]/div/div[1]/div[3]/div/div/div/article[1]/div/div/input").sendKeys("New Order Flow");
		driver.findElementByXPath("//*[@id=\"targetlang\"]").click();
		driver.findElementByXPath("//*[@id=\"myModal\"]/div/div/div[2]/ul/li[1]/input").click();
		driver.findElementByXPath("//*[@id=\"myModal\"]/div/div/div[3]/button").click();
		driver.findElementByXPath("//*[@id=\"industrybtn\"]").click();
		Thread.sleep(2000);  
		driver.findElementByXPath("//input[@name=\'Aerospace\']").click();;
		((JavascriptExecutor) driver).executeScript("scroll(0, 250);");
		Thread.sleep(3000);
		//driver.findElementByXPath("//*[@id=\"myModal\"]/div/div/div[1]/button").click();
		driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div[2]/div[2]/div/div/section[2]/div/div[1]/div[3]/div/div/div/article[4]/div[2]/a[2]").click();
		driver.findElementByXPath("//*[@id=\"tText\"]/textarea").sendKeys("hi my name is tarun");
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div[2]/div[2]/div/div/section[2]/div/div[1]/div[3]/div/div/div/article[5]/div/a/img").click();
		driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div/div[2]/div/div/section[2]/div/div[1]/div[1]/div/div/div/div/article[1]/div/div/input").sendKeys("Tag name");
	    WebElement testDropDown = driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div[2]/div[2]/div/div/section[2]/div/div[1]/div[1]/div/div/div/div/article[2]/div/div/select");
	    Select politness=new Select(testDropDown);
	    politness.selectByValue("47");
	    
	    driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div[2]/div[2]/div/div/section[2]/div/div[1]/div[1]/div/div/div/div/article[3]/div/div/textarea").sendKeys("This is instruction");
	    driver.findElementByXPath("//*[@id=\"checkbox2\"]").click();
	    driver.findElementByXPath("//*[@id=\"terms_conditions\"]").click();
	    driver.findElementByXPath("//*[@id=\"main-wrapper\"]/div[2]/div[2]/div/div/section[2]/div/div[2]/aside/div/article/div[3]/div/div[2]/a/div[2]").click();
	    
		
			
				
	}

}
